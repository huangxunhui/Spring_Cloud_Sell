package com.hxh.user.repository;

import com.hxh.user.dateobject.UserInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author huangxunhui
 * Date: Created in 18/9/30 下午4:34
 * Utils: Intellij Idea
 * Description:
 */
@Repository
public interface UserInfoRepository extends JpaRepository<UserInfo,String> {

    /**
     * 通过openid查询用户信息
     * @param openid openId
     * @return 返回用户信息
     */
    UserInfo findByOpenid(String openid);

}
