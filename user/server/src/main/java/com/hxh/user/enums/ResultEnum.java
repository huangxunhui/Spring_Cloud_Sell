package com.hxh.user.enums;

import lombok.Getter;

/**
 * @author huangxunhui
 * Date: Created in 18/8/25 下午10:46
 * Utils: Intellij Idea
 * Description: 返回提示信息枚举对象
 */
@Getter
public enum ResultEnum {

    /**
     * 登录失败
     */
    LOGIN_FAIL(1, "登录失败"),

    ROLE_ERROR(2, "角色权限有误"),

    ;

    private Integer code;

    private String message;

    ResultEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
