package com.hxh.user.constant;

/**
 * @author huangxunhui
 * Date: Created in 18/9/30 下午5:02
 * Utils: Intellij Idea
 * Description:
 */
public interface CookieConstant {

    String TOKEN = "token";


    String OPENID = "openid";

    /**
     * 过期时间
     */
     Integer EXPIRE = 7200;


}
