package com.hxh.user.dateobject;

import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * @author huangxunhui
 * Date: Created in 18/9/30 下午4:27
 * Utils: Intellij Idea
 * Description:
 */
@Data
@Entity
@DynamicUpdate
public class UserInfo {

    /**
     * 用户Id
     */
    @Id
    private String id;

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 微信openId
     */
    private String openid;

    /**
     * 角色
     */
    private Integer role;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 更新时间
     */
    private String updateTime;

}
