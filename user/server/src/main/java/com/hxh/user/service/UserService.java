package com.hxh.user.service;

import com.hxh.user.dateobject.UserInfo;
import org.springframework.stereotype.Service;

/**
 * @author huangxunhui
 * Date: Created in 18/9/30 下午4:36
 * Utils: Intellij Idea
 * Description:
 */
public interface UserService {

    /**
     * 通过openid查询用户信息
     * @param openid openId
     * @return 返回用户信息
     */
    UserInfo findByOpenid(String openid);

}
