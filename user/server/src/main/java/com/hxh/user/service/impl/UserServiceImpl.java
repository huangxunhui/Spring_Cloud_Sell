package com.hxh.user.service.impl;

import com.hxh.user.dateobject.UserInfo;
import com.hxh.user.repository.UserInfoRepository;
import com.hxh.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author huangxunhui
 * Date: Created in 18/9/30 下午4:37
 * Utils: Intellij Idea
 * Description:
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserInfoRepository userInfoRepository;

    @Override
    public UserInfo findByOpenid(String openid) {
        return userInfoRepository.findByOpenid(openid);
    }
}
