package com.hxh.user.controller;

import com.hxh.user.VO.ResultVO;
import com.hxh.user.constant.CookieConstant;
import com.hxh.user.constant.RedisConstant;
import com.hxh.user.dateobject.UserInfo;
import com.hxh.user.enums.ResultEnum;
import com.hxh.user.enums.RoleEnum;
import com.hxh.user.service.UserService;
import com.hxh.user.utils.CookieUtil;
import com.hxh.user.utils.ResultVoUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * @author huangxunhui
 * Date: Created in 18/9/30 下午4:39
 * Utils: Intellij Idea
 * Description:
 */

@RestController
@RequestMapping("/login")
public class LoginController {

    @Autowired
    private UserService userService;

    @Autowired
    private StringRedisTemplate redisTemplate;

    @GetMapping("/buyer")
    public ResultVO buyer(@RequestParam("openid") String openid , HttpServletResponse response){

        //openid和数据库进行匹配
        UserInfo userInfo = userService.findByOpenid(openid);
        if(userInfo == null){
            return ResultVoUtil.error(ResultEnum.LOGIN_FAIL);
        }
        //判断角色
        if(!RoleEnum.BUYER.getCode().equals(userInfo.getRole())){
            return ResultVoUtil.error(ResultEnum.ROLE_ERROR);
        }
        //cookie里设置openid = abc
        CookieUtil.set(response, CookieConstant.OPENID, openid ,CookieConstant.EXPIRE);

        return ResultVoUtil.success();
    }

    @GetMapping("/seller")
    public ResultVO seller(@RequestParam("openid") String openid ,
                           HttpServletRequest request,
                           HttpServletResponse response){
        //判断是否登录
        Cookie cookie = CookieUtil.get(request, CookieConstant.TOKEN);
        if(cookie != null &&
                !StringUtils.isEmpty(redisTemplate.opsForValue().get(
                        String.format(RedisConstant.TOKEN_TEMPLATE,cookie.getValue())))){
            return ResultVoUtil.success();
        }

        //openid和数据库进行匹配
        UserInfo userInfo = userService.findByOpenid(openid);
        if(userInfo == null){
            return ResultVoUtil.error(ResultEnum.LOGIN_FAIL);
        }
        //判断角色
        if(!RoleEnum.SELLER.getCode().equals(userInfo.getRole())){
            return ResultVoUtil.error(ResultEnum.ROLE_ERROR);
        }

        //redis 设置key = UUID ，value = xyz
        String token = UUID.randomUUID().toString();
        Integer expire = CookieConstant.EXPIRE;
        redisTemplate.opsForValue().set(String.format(RedisConstant.TOKEN_TEMPLATE,token),
                openid,expire, TimeUnit.SECONDS);

        //cookie里设置openid = abc
        CookieUtil.set(response, CookieConstant.TOKEN, token ,CookieConstant.EXPIRE);

        return ResultVoUtil.success();
    }

}
