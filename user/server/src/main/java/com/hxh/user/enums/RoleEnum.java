package com.hxh.user.enums;

import lombok.Getter;

/**
 * @author huangxunhui
 * Date: Created in 18/9/30 下午4:52
 * Utils: Intellij Idea
 * Description: 角色枚举
 */
@Getter
public enum  RoleEnum {

    /**
     * 角色枚举
     */

    BUYER(1,"买家"),

    SELLER(2,"卖家"),
    ;

    private Integer code;

    private String message;

    RoleEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
