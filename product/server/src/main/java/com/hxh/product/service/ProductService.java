package com.hxh.product.service;

import com.hxh.product.common.DecreaseStockInput;
import com.hxh.product.dataobject.ProductInfo;
import com.hxh.product.dto.CartDTO;

import java.util.List;

/**
 * @author huangxunhui
 * Date: Created in 18/8/25 下午10:46
 * Utils: Intellij Idea
 * Description: 商品服务
 */
public interface ProductService {

    /**
     * 查询所有在架商品列表
     */
    List<ProductInfo> findUpAll();

    /**
     * 查询商品列表
     * @param productIdList 商品Id列表
     * @return 返回商品列表
     */
    List<ProductInfo> findList(List<String> productIdList);

    /**
     * 扣库存
     * @param decreaseStockInputList
     */
    void decreaseStock(List<DecreaseStockInput> decreaseStockInputList);
}
