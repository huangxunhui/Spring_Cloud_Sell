package com.hxh.product.enums;

import lombok.Getter;

/**
 * @author huangxunhui
 * Date: Created in 18/8/25 下午10:46
 * Utils: Intellij Idea
 * Description: 商品上下架状态
 */
@Getter
public enum ProductStatusEnum {

    /**
     * 商品上下架状态
     */
    UP(0, "在架"),

    DOWN(1, "下架"),
    ;

    private Integer code;

    private String message;

    ProductStatusEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
