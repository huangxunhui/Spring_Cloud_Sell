package com.hxh.product.dto;

import lombok.Data;

/**
 * @author huangxunhui
 * Date: Created in 18/9/11 下午6:07
 * Utils: Intellij Idea
 * Description:
 */
@Data
public class CartDTO {

    /**
     * 商品Id
     */
    private String productId;

    /**
     * 商品数量
     */
    private Integer productQuantity;

}
