package com.hxh.product.repository;

import com.hxh.product.dataobject.ProductInfo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author huangxunhui
 * Date: Created in 18/8/25 下午10:46
 * Utils: Intellij Idea
 * Description: 商品信息资源类
 */
public interface ProductInfoRepository extends JpaRepository<ProductInfo, String>{

    /**
     * 查询在上架的商品
     * @param productStatus 商品状态
     * @return 返回商品列表
     */
    List<ProductInfo> findByProductStatus(Integer productStatus);


    List<ProductInfo> findByProductIdIn(List<String> productIdList);

}
