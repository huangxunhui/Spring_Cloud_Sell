package com.hxh.product.service;

import com.hxh.product.dataobject.ProductCategory;


import java.util.List;

/**
 * @author huangxunhui
 * Date: Created in 18/8/25 下午10:46
 * Utils: Intellij Idea
 * Description: 类目服务
 */
public interface CategoryService {
    /**
     * 获取类目type列表
     * @param categoryTypeList 类目Type列表
     * @return 返回商品类目列表
     */
    List<ProductCategory> findByCategoryTypeIn(List<Integer> categoryTypeList);


}
