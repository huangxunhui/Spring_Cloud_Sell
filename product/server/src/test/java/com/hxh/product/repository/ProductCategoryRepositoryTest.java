package com.hxh.product.repository;

import com.hxh.product.dataobject.ProductCategory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author huangxunhui
 * Date: Created in 18/8/25 下午11:12
 * Utils: Intellij Idea
 * Description:
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class ProductCategoryRepositoryTest {

    @Autowired
    private ProductCategoryRepository repository;

    @Test
    public void findByCategoryTypeIn() {
        List<ProductCategory> productCategoryList = repository.findByCategoryTypeIn(Arrays.asList(1, 2));
        productCategoryList.forEach(productInfo -> System.out.println("productInfo = " + productInfo));
        assertTrue(productCategoryList.size() > 0);
    }
}