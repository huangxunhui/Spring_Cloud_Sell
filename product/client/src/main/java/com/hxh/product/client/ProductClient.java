package com.hxh.product.client;

import com.hxh.product.common.DecreaseStockInput;
import com.hxh.product.common.ProductInfoOutput;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import java.util.List;

/**
 * @author huangxunhui
 * Date: Created in 18/9/11 下午5:41
 * Utils: Intellij Idea
 * Description:
 */
@FeignClient(name = "product" , fallback = ProductClient.ProductClientFallback.class)
public interface ProductClient {

    /**
     * 测试
     * @return 返回
     */
    @GetMapping("msg")
    String productMsg();

    /**
     * 获取商品列表
     * @param productIdList 商品Id列表
     * @return 返回商品Id
     */
    @PostMapping("/product/listForOrder")
    List<ProductInfoOutput> listForOrder(List<String> productIdList);

    /**
     * 扣库存
     * @param decreaseStockInputList 购物车列表
     */
    @PostMapping("/product/decreaseStock")
    void decreaseStock(@RequestBody List<DecreaseStockInput> decreaseStockInputList);

    @Component
    static class ProductClientFallback implements ProductClient{


        @Override
        public String productMsg() {
            return null;
        }

        @Override
        public List<ProductInfoOutput> listForOrder(List<String> productIdList) {
            return null;
        }

        @Override
        public void decreaseStock(List<DecreaseStockInput> decreaseStockInputList) {

        }
    }
}

