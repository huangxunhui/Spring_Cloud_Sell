package com.hxh.zuul;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author huangxunhui
 * Date: Created in 18/9/29 下午3:27
 * Utils: Intellij Idea
 * Description:
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class TestValue {

    @Value("${zuul.routes.product}")
    private String value;

    @Test
    public void getValue(){
        System.out.println("value = " + value);
    }

}
