//package com.hxh.zuul.filter;
//
//import com.hxh.zuul.constant.RedisConstant;
//import com.hxh.zuul.utils.CookieUtil;
//import com.netflix.zuul.ZuulFilter;
//import com.netflix.zuul.context.RequestContext;
//import com.netflix.zuul.exception.ZuulException;
//import org.apache.commons.lang.StringUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.redis.core.StringRedisTemplate;
//import org.springframework.http.HttpStatus;
//import org.springframework.stereotype.Component;
//
//import javax.servlet.http.Cookie;
//import javax.servlet.http.HttpServletRequest;
//
//import static org.springframework.cloud.netflix.zuul.filters.support.FilterConstants.PRE_DECORATION_FILTER_ORDER;
//import static org.springframework.cloud.netflix.zuul.filters.support.FilterConstants.PRE_TYPE;
//
///**
// * @author huangxunhui
// * Date: Created in 18/9/29 下午3:58
// * Utils: Intellij Idea
// * Description: 权限拦截区分买家和卖家
// */
//@Component
//public class AuthFilter extends ZuulFilter {
//
//    /**
//     * 订单创建
//     */
//    private final String ORDER_CREATE = "/order/order/create";
//
//    /**
//     * 订单完结
//     */
//    private final String ORDER_FINISH = "/order/order/finish";
//
//    @Autowired
//    private StringRedisTemplate redisTemplatel;
//
//    @Override
//    public String filterType() {
//        return PRE_TYPE;
//    }
//
//    @Override
//    public int filterOrder() {
//        return PRE_DECORATION_FILTER_ORDER - 1;
//    }
//
//    @Override
//    public boolean shouldFilter() {
//        return true;
//    }
//
//    @Override
//    public Object run() throws ZuulException {
//        RequestContext requestContext = RequestContext.getCurrentContext();
//        HttpServletRequest request = requestContext.getRequest();
//        /**
//         * /order/create 只能买家访问
//         * /order/finish 只能卖家访问
//         * /product/list 都可以访问
//         */
//        if(ORDER_CREATE.equals(request.getRequestURI())){
//            Cookie cookie = CookieUtil.get(request, "openid");
//            if(cookie == null || StringUtils.isEmpty(cookie.getValue())){
//                requestContext.setSendZuulResponse(false);
//                requestContext.setResponseStatusCode(HttpStatus.UNAUTHORIZED.value());
//            }
//        }
//
//        if(ORDER_FINISH.equals(request.getRequestURI())){
//            Cookie cookie = CookieUtil.get(request, "token");
//            if(cookie == null
//                    || StringUtils.isEmpty(cookie.getValue())
//                    || StringUtils.isEmpty(redisTemplatel.opsForValue().get(String.format(RedisConstant.TOKEN_TEMPLATE,cookie.getValue())))
//            ){
//                requestContext.setSendZuulResponse(false);
//                requestContext.setResponseStatusCode(HttpStatus.UNAUTHORIZED.value());
//            }
//        }
//
//        return null;
//    }
//}
