package com.hxh.order;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author huangxunhui
 * Date: Created in 18/9/28 上午11:45
 * Utils: Intellij Idea
 * Description: 消息发送
 */
@Slf4j
@Component
@SpringBootTest
@RunWith(SpringRunner.class)
public class MQSenderTest {

    @Autowired
    private AmqpTemplate amqpTemplate;

    @Test
    public void send() {
        log.info("开始发送--------》》》》》》》》");
        amqpTemplate.convertAndSend("myQueue", "hello rabbitMq");
    }

    @Test
    public void sendOrder() {
        amqpTemplate.convertAndSend("myOrder", "computer","hello rabbitMq");
    }


}
