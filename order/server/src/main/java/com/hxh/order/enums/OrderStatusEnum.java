package com.hxh.order.enums;

import lombok.Getter;

/**
 * @author huangxunhui
 * Date: Created in 18/9/2 下午2:54
 * Utils: Intellij Idea
 * Description:
 */
@Getter
public enum OrderStatusEnum {
    /**
     * 订单
     */

    NEW(0, "新订单"),
    FINISHED(1, "完结"),
    CANCEL(2, "取消"),
    ;
    private Integer code;

    private String message;

    OrderStatusEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
