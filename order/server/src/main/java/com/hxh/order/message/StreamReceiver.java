package com.hxh.order.message;

import com.hxh.order.dto.OrderDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.stereotype.Component;

/**
 * @author huangxunhui
 * Date: Created in 18/9/28 下午3:11
 * Utils: Intellij Idea
 * Description:
 */
@Slf4j
@Component
@EnableBinding(StreamClient.class)
public class StreamReceiver {

    @StreamListener(StreamClient.INPUT)
    public void processInput(Object message){
        log.info("Input StreamReceiver:{}" ,message);
    }

    @StreamListener(StreamClient.OUTPUT)
    public void processOutput(OrderDTO message){
        log.info("output StreamReceiver:{}" ,message);
    }

}
