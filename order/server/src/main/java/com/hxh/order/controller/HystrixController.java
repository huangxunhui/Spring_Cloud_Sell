package com.hxh.order.controller;

import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

/**
 * @author huangxunhui
 * Date: Created in 18/10/2 上午10:30
 * Utils: Intellij Idea
 * Description:
 */
@RestController
@DefaultProperties(defaultFallback = "defaultFallback")
public class HystrixController {

    //超时配置
//    @HystrixCommand(commandProperties = {
//            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds" , value="3000")
//    })
    //熔断机制
//    @HystrixCommand(commandProperties = {
//            /*设置熔断*/
//            @HystrixProperty(name = "circuitBreaker.enabled" , value="true"),
//            @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold" , value="10"),
//            @HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds" , value="1000"),
//            @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage" , value="60")
//    })

    /**
     *
     * @param number
     * @return
     */
    @HystrixCommand
    @GetMapping("/getProductInfoList")
    public String getProductInfoList(@RequestParam("number") Integer number){
        if(number %2 == 0 ){
            return "success";
        }
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.postForObject("http://localhost:8082/product/listForOrder",
                Arrays.asList("110"),
                String.class);
    }

    private String fallback(){
        return "太拥挤了，请稍后再试";
    }

    private String defaultFallback(){
        return "默认提示: 太拥挤了，请稍后再试";
    }
}
