package com.hxh.order.dto;

import lombok.Data;

/**
 * @author huangxunhui
 * Date: Created in 18/9/2 下午2:54
 * Utils: Intellij Idea
 * Description:
 */
@Data
public class CartDTO {
    /**
     * 商品id
     */
    private String productId;

    /**
     * 商品数量
     */
    private Integer productQuantity;

    public CartDTO() {
    }

    public CartDTO(String productId, Integer productQuantity) {
        this.productId = productId;
        this.productQuantity = productQuantity;
    }
}
