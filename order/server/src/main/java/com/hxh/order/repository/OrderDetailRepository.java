package com.hxh.order.repository;

import com.hxh.order.dataobject.OrderDetail;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author huangxunhui
 * Date: Created in 18/8/25 下午10:46
 * Utils: Intellij Idea
 * Description: 订单详情
 */
public interface OrderDetailRepository extends JpaRepository<OrderDetail, String> {

    /**
     * 通过订单号查询订单详情
     * @param orderId 订单号
     * @return 返回订单详情列表
     */
    List<OrderDetail> findByOrderId(String orderId);

}
