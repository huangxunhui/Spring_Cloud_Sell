package com.hxh.order.controller;

import com.hxh.order.dto.OrderDTO;
import com.hxh.order.message.StreamClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * @author huangxunhui
 * Date: Created in 18/9/28 下午3:13
 * Utils: Intellij Idea
 * Description:
 */
@RestController
public class SendMessageController {

    @Autowired
    private StreamClient streamClient;

//    @GetMapping("/sendMessage")
//    public void process(){
//        streamClient.outputMessage().send(MessageBuilder.withPayload("发送消息").build());
//    }

    @GetMapping("/sendMessage")
    public void process(){
        OrderDTO orderDTO = new OrderDTO();
        orderDTO.setOrderId("123456");
        streamClient.outputMessage().send(MessageBuilder.withPayload(orderDTO).build());
    }


}
