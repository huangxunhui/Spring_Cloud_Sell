package com.hxh.order.repository;

import com.hxh.order.dataobject.OrderMaster;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author huangxunhui
 * Date: Created in 18/8/25 下午10:46
 * Utils: Intellij Idea
 * Description:订单
 */
public interface OrderMasterRepository extends JpaRepository<OrderMaster, String> {
}
