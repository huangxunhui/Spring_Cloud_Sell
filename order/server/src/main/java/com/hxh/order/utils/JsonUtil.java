package com.hxh.order.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

/**
 * @author huangxunhui
 * Date: Created in 18/8/25 下午10:46
 * Utils: Intellij Idea
 * Description: json工具类
 */
public class JsonUtil {

	private static ObjectMapper objectMapper = new ObjectMapper();

	/**
	 * 转换为json字符串
	 * @param object
	 * @return
	 */
	public static String toJson(Object object) {
		try {
			return objectMapper.writeValueAsString(object);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * json串转换为 对象
	 * @param string json 串
	 * @param typeReference 对象的类型
	 * @return 返回对象
	 */
	public static Object formJson(String string , TypeReference typeReference){
		try {
			return objectMapper.readValue(string,typeReference);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}
