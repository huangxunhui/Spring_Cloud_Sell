package com.hxh.order.message;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @author huangxunhui
 * Date: Created in 18/9/28 上午11:42
 * Utils: Intellij Idea
 * Description: 接受MQ消息
 */
@Slf4j
@Component
public class MqReceiver {

    //2.@RabbitListener(queuesToDeclare = @Queue("myQueue"))
    //3.自动创建Exchange 和Queue绑定
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue("myQueue"),
            exchange = @Exchange("myExchange")
    ))
    public void process(String message){
        log.info("MqReceiver : {}",message);
    }

    /**
     * 数码服务供应商，接收消息
     * @param message 消息
     */
    @RabbitListener(bindings = @QueueBinding(
            exchange = @Exchange("myOrder"),
            key = "computer",
            value = @Queue("computerOrder")

    ))
    public void processComputer(String message){
        log.info("Computer MqReceiver : {}",message);
    }


    /**
     * 水果供应商，接收消息
     * @param message 消息
     */
    @RabbitListener(bindings = @QueueBinding(
            exchange = @Exchange("myOrder"),
            key = "fruit",
            value = @Queue("computerOrder")

    ))
    public void processFruit(String message){
        log.info("Fruit MqReceiver : {}",message);
    }

}
