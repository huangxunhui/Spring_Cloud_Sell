package com.hxh.order.message;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

/**
 * @author huangxunhui
 * Date: Created in 18/9/28 下午3:09
 * Utils: Intellij Idea
 * Description:
 */
public interface StreamClient {

    String INPUT = "inputMyMessage";

    String OUTPUT = "outputMyMessage";

    /**
     * 消息输入
     * @return
     */
    @Input(StreamClient.INPUT)
    SubscribableChannel inputMessage();

    /**
     * 消息输出
     * @return
     */
    @Output(StreamClient.OUTPUT)
    MessageChannel outputMessage();

}
